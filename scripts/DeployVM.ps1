
if ( $AzureCred -eq $null )
{
    $AzureCred = Get-Credential -Message "Please enter your Azure Credentials" -UserName "stuart.mace@powerco.co.nz"
}

$azureAccount = Login-AzureRmAccount -Credential $AzureCred

if ( $LocalAdminCred -eq $null )
{
    $LocalAdminCred = Get-Credential -Message "Type the name and password of the local administrator account." -UserName "stu"
}

$location = "southeastasia"
$resourceGroup = $storageAccountName = "stulab"

#region: Set up Azure

Write-Output "Checking ResourceGroup $resourceGroup exists"
if ((Get-AzureRmResourceGroup $resourceGroup) -eq $null)
{
    New-AzureRmResourceGroup -Name $resourceGroup -Location $location
}

# not strictly needed as we'll be using managed disks
Write-Output "Checking storage account $storageAccountName exists"
$storageAccount = Get-AzureRmStorageAccount -ResourceGroupName $resourceGroup -Name $storageAccountName -ErrorAction SilentlyContinue
if ($storageAccount -eq $null) 
{
    if (Get-AzureRmStorageAccountNameAvailability $storageAccountName)
    {
         $storageAccount = New-AzureRmStorageAccount -ResourceGroupName $resourceGroup -Name $storageAccountName `
                                                     -SkuName "Standard_LRS" -Kind "Storage" -Location $location
    }
}

$subnetName = "$resourceGroup-10.0.0.0-24"
$vnetName = "$resourceGroup-vnet-10.0.0.0-16"
Write-Output "Checking virtual network $vnetName exists"
$vnet = Get-AzureRmVirtualNetwork -ResourceGroupName $resourceGroup -Name $vnetName -ErrorAction SilentlyContinue


if ($vnet)
{
    $subnet = Get-AzureRmVirtualNetworkSubnetConfig -VirtualNetwork $vnet -Name $subnetName
}
else
{
    $subnet = New-AzureRmVirtualNetworkSubnetConfig -Name $subnetName -AddressPrefix 10.0.0.0/24

    $vnet = New-AzureRmVirtualNetwork -Name $vnetName -ResourceGroupName $resourceGroup `
                                      -Location $location -AddressPrefix 10.0.0.0/16 -Subnet $subnet
}

#endregion

#region: Set up VM

$VMname = "chef"
$Size = "Standard_DS1_v2"

$publicIPname = "$VMname-publicIP1"
Write-Output "Checking public IP $publicIPname exists"
$publicIP = Get-AzureRmPublicIpAddress -Name $publicIPname -ResourceGroupName $ResourceGroup -ErrorAction SilentlyContinue

if (-not $publicIP)
{
    $publicIp = New-AzureRmPublicIpAddress -Name $publicIPname -ResourceGroupName $ResourceGroup `
         -Location $location -AllocationMethod Dynamic
}

$NICname = "$VMname-nic"
Write-Output "Checking NIC $NICname exists"
$NIC = Get-AzureRmNetworkInterface -Name $NICname -ResourceGroupName $ResourceGroup -ErrorAction SilentlyContinue

if (-not $NIC)
{

    $NIC = New-AzureRmNetworkInterface -Name $NICname -ResourceGroupName $ResourceGroup `
         -Location $location -SubnetId $Vnet.Subnets[0].Id -PublicIpAddressId $PublicIp.Id
}

$VMconfig = New-AzureRmVMConfig -VMName $VMname -VMSize $Size

$VMconfig = Set-AzureRmVMOperatingSystem -VM $VMconfig -Windows -ComputerName $VMname -Credential $LocalAdminCred `
     -ProvisionVMAgent -EnableAutoUpdate

# Get-AzureRmVMImagePublisher -Location $location 
# Get-AzureRMVMImageOffer -Location $location -Publisher "MicrosoftWindowsServer"
# Get-AzureRMVMImageSku -Location $location -Publisher "MicrosoftWindowsServer" -Offer "WindowsServer" | select Skus, Offer, PublisherName

<#

Skus                            Offer         PublisherName         
----                            -----         -------------         
2008-R2-SP1                     WindowsServer MicrosoftWindowsServer
2008-R2-SP1-BYOL                WindowsServer MicrosoftWindowsServer
2012-Datacenter                 WindowsServer MicrosoftWindowsServer
2012-Datacenter-BYOL            WindowsServer MicrosoftWindowsServer
2012-R2-Datacenter              WindowsServer MicrosoftWindowsServer
2012-R2-Datacenter-BYOL         WindowsServer MicrosoftWindowsServer
2016-Datacenter                 WindowsServer MicrosoftWindowsServer
2016-Datacenter-with-Containers WindowsServer MicrosoftWindowsServer
2016-Nano-Server                WindowsServer MicrosoftWindowsServer

#>

$VMconfig = Set-AzureRmVMSourceImage -VM $VMconfig -PublisherName "MicrosoftWindowsServer" `
                                     -Offer "WindowsServer" -Skus "2016-Datacenter" -Version "latest"

$VMconfig = Add-AzureRmVMNetworkInterface -VM $VMconfig -Id $NIC.Id


$VMconfig = Set-AzureRmVMOSDisk -VM $VMconfig -Name "$VMname-OsDisk1" -StorageAccountType StandardLRS -DiskSizeInGB 64 -CreateOption FromImage -Caching ReadWrite

Write-Output "Checking VM $VMname exists"
$VM = (Get-AzureRMVM -Name $VMconfig.Name -ResourceGroupName $resourceGroup -ErrorAction SilentlyContinue)
if (-not $VM)
{
    $VM = New-AzureRmVM -ResourceGroupName $ResourceGroup -Location $location -VM $VMconfig
}

$publicIP = Get-AzureRmPublicIpAddress -Name $publicIPname -ResourceGroupName $ResourceGroup -ErrorAction SilentlyContinue

$arg = "/v " + $PublicIp.IpAddress

saps "$env:windir\system32\mstsc.exe" $arg

<#
Remove-AzureRmVM -ResourceGroupName $resourceGroup -Name $VMconfig.Name -Force

Remove-AzureRmNetworkInterface -Name $NICname -ResourceGroupName $ResourceGroup -Force

Remove-AzureRmPublicIpAddress -Name $publicIPname -ResourceGroupName $ResourceGroup -Force

Remove-AzureRmDisk -DiskName "$VMname-OSDisk1" -ResourceGroupName $ResourceGroup -Force 
#>

$resourceGroup = $storageAccountName = "stulab"
Get-AzureRMVM -ResourceGroupName $resourceGroup | ForEach {

    Stop-AzureRMVM -Name $_.Name -ResourceGroupName $resourceGroup -Force

}
